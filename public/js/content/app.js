$(function() {
    /**
     * Cargamos select de destino según búsqueda
     */
	$('#destino').select2({
		placeholder: 'Destino',
		minimumInputLength: 3,
		ajax: {
		    url: 'https://testsoat.interseguro.com.pe/talentfestapi/destinos/eur',
		    dataType: 'json',
		    type: 'GET',
		    data: function (param) {
		    	return '';
		    },
		    processResults: function (data) {
				return {
					results: $.map(data, function(destino) {
						return { id: destino, text: destino };
					})
				};
		    }
		}
	});

	/**
     * Método que permite ingresar solo números enteros
     */
    $(document).on('keyup', '.only-number', function(e) {
		var textarea = this;
		var key = e.which || e.keyCode;

		if(!key || key == 229) {
			var ss = textarea.selectionStart - 1;
			var ssv = ss || 0;
			var char = textarea.value.substr(ssv,1);
			key = char.charCodeAt(0);

			if (!(
				$.inArray(key, [8, 9, 13, 16, 37, 38, 39, 40, 46]) != -1 || 
				((48 <= key && key <= 57) || key == 190)
			)) {
				var nuevoValor = textarea.value.replace(new RegExp(char, 'gi'), '');
				textarea.value = nuevoValor;
			}
		} else {
			var char = e.key;

			if (!(
				$.inArray(key, [8, 9, 13, 16, 37, 38, 39, 40, 46]) != -1 || 
				(48 <= key && key <= 57) || 
				(96 <= key && key <= 105)
			)) {
				var nuevoValor = textarea.value.replace(new RegExp(char, 'gi'), '');
				textarea.value = nuevoValor;
			}
		}

		return true;
    });

    /**
     * Cargamos plugin datepicker
     */
	$('.input-date').datepicker({
	    format: 'dd/mm/yyyy',
	    language: 'es',
	    startDate: '0d',
	    todayBtn: true,
	    autoclose: true,
	    todayHighlight: true,
	    enableOnReadonly: true
	});

    /**
     * Validamos el formulario
     */
	$(document).on('submit', '#form-cotizador', function(e){
		e.preventDefault();
		e.stopPropagation();

		var rpta = true;
		rpta = validarSiExisteCampoVacio(rpta);
		rpta = validarValorNumerico(rpta);
		rpta = validarValorFecha(rpta);
		rpta = validarSiIdaMayorVuelta(rpta);
		rpta = generarCotizacion(rpta);
		return rpta;
	});

    /**
     * Regresamos al formulario del cotizador
     */
	$(document).on('click', '.btn-return', function(){
		$('#destino, #numero-pasajero').val('');
		$('#fecha-ida, #fecha-vuelta').val(hoy);
		$('.price-result').hide();
		$('.price-form').show();
		return true;
	});
});

/**
 * Validamos SI existen campos vacios en el formulario
 *
 * @author Harvey Sanchez <havo.sanchez@gmail.com>
 * @since 19-12-2019
 * @version 1.0
 * @param boolean Valor inicial de la validación
 * @return boolean Resultado de la validación de los campos
 */
function validarSiExisteCampoVacio(rpta)
{
	var form = $('#form-cotizador');
	var inputValue;

	// Recorremos todos los input del formulario
	form.find('select, input').each(function() {
		inputValue = $(this).val();

		if (inputValue == '') {
			form.addClass('was-validated');
			rpta = false;
			return rpta;
		}
	});

	return rpta;
}

/**
 * Validamos SI el campo numerico tiene el valor correcto
 *
 * @author Harvey Sanchez <havo.sanchez@gmail.com>
 * @since 19-12-2019
 * @version 1.0
 * @param boolean Resultado de otras validaciones
 * @return boolean Resultado de la validación de los campos
 */
function validarValorNumerico(rpta)
{
	// Validamos SI todo va bien
	if (rpta) {
		var pasajero = $('#numero-pasajero').val();

		if (!$.isNumeric(pasajero)) {
			$('#form-cotizador').addClass('was-validated');
			rpta = false;
		}
	}

	return rpta;
}

/**
 * Validamos SI el valor de las fechas son correctas
 *
 * @author Harvey Sanchez <havo.sanchez@gmail.com>
 * @since 19-12-2019
 * @version 1.0
 * @param boolean Resultado de otras validaciones
 * @param object Formulario del cotizador
 * @return boolean Resultado de la validación de los campos
 */
function validarValorFecha(rpta)
{
	// Validamos SI todo va bien
	if (rpta) {
		var RegExPattern = /^\d{1,2}\/\d{1,2}\/\d{2,4}$/;
		var form = $('#form-cotizador');
		var inputValue;

		// Recorremos todos los input de fecha
		form.find("input[id^='fecha-']").each(function() {
			inputValue = $(this).val();

			if (!inputValue.match(RegExPattern)) {
				$('.alert-price').html('Seleccione una fecha de correcta.').fadeIn(1000);
				$('.alert-price').delay(4000).fadeOut('normal', function(){
					$(this).remove();
				});
				rpta = false;
				return rpta;
			}
		});
	}

	return rpta;
}

/**
 * Validamos SI la fecha de ida es mayor a la fecha de vuelta
 *
 * @author Harvey Sanchez <havo.sanchez@gmail.com>
 * @since 19-12-2019
 * @version 1.0
 * @param boolean Resultado de otras validaciones
 * @return boolean Resultado de la validación de los campos
 */
function validarSiIdaMayorVuelta(rpta)
{
	// Validamos SI todo va bien
	if (rpta) {
		var ida = $('#fecha-ida').val();
		var vuelta = $('#fecha-vuelta').val();

		if (ida > vuelta) {
			$('.alert-price').html('Seleccione una fecha de ida correcta.').fadeIn(1000);
			$('.alert-price').delay(4000).fadeOut('normal', function(){
				$(this).remove();
			});
			rpta = false;
		}
	}

	return rpta;
}

/**
 * Generamos cotización
 *
 * @author Harvey Sanchez <havo.sanchez@gmail.com>
 * @since 19-12-2019
 * @version 1.0
 * @param boolean Resultado de otras validaciones
 * @return boolean Resultado de la validación de los campos
 */
function generarCotizacion(rpta)
{
	// Validamos SI todo va bien
	if (rpta) {
		$('button').attr('disabled', true);

		var data = {};
		data['destino'] = $('#destino').val();
		data['cantidad_pasajeros'] = $('#numero-pasajero').val();
		data['fecha_partida'] = $('#fecha-ida').val();
		data['fecha_retorno'] = $('#fecha-vuelta').val();

		$.ajaxSetup({
			headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') }
		});

		$.ajax({
			type: 'POST',
			url: 'https://testsoat.interseguro.com.pe/talentfestapi/cotizacion',
			data: data,
			dataType: 'json',
			cache: false
		})
		.done(function(data, textStatus, jqXHR) {
			rpta = mostrarTablaResultadoCotizacion(data);
		})
		.fail(function(jqXHR, textStatus, errorThrown) {
			rpta = mensajeFail();
		});
	}

	return rpta;
}

/**
 * Mostramos rsultado de cotización en tabla
 *
 * @author Harvey Sanchez <havo.sanchez@gmail.com>
 * @since 19-12-2019
 * @version 1.0
 * @param json datos revueltos por el servicio
 * @return boolean Resultado de petición
 */
function mostrarTablaResultadoCotizacion(data) {
	var table;
	table = cargarCabeceraTabla(table, data);
	table = cargarCuerpoTabla(table, data);

	$('button').attr('disabled', false);
	$('.table-price-result').html(table);
	$('.price-form').hide();
	$('.price-result').show();
	return true;
}

/**
 * Cargamos cabecera de la tabla de resultados
 *
 * @author Harvey Sanchez <havo.sanchez@gmail.com>
 * @since 19-12-2019
 * @version 1.0
 * @param table Variable que será concatenada con la cabecera
 * @param json datos revueltos por el servicio
 * @return string Cabecera de tabla
 */
function cargarCabeceraTabla(table, data) {
	table += '<thead>';
	table += '<tr>';
	table += '<th scope="col" rowspan="2">Cobertura/\Plan</th>';

	for (var h = 0; h < data.length; h++) {
		table += '<th scope="col">'+data[h].tipo_plan+'</th>';
	}

	table += '</tr><tr>';

	for (var h = 0; h < data.length; h++) {
		table += '<th scope="col">$ ';
		table += number_format(data[h].precio_plan, 2, '.', '');
		table += '</th>';
	}

	table += '</tr>';
	table += '</thead>';
	return table;
}

/**
 * Cargamos cuerpo de la tabla de resultados
 *
 * @author Harvey Sanchez <havo.sanchez@gmail.com>
 * @since 19-12-2019
 * @version 1.0
 * @param table Variable que será concatenada con el cuerpo
 * @param json datos revueltos por el servicio
 * @return string cuerpo de tabla
 */
function cargarCuerpoTabla(table, data)
{
	var fila = data[0].caracteristicas;

	table += '<tbody>';

	for (var f = 0; f < fila.length; f++) {
		table += '<tr>';
		table += '<td class="text-left" scope="row">'+fila[f].caracteristica+'</td>';

		for (var b = 0; b < data.length; b++) {
			table += '<td><input class="form-check-input" type="checkbox" disabled ';
			if (data[b].caracteristicas[f].aplica) table += 'checked'
			table += '></td>';
		}

		table += '</tr>';
	}

	table += '</tbody>';
	return table;
}

/**
 * Damos formato a un número
 *
 * @author Harvey Sanchez <havo.sanchez@gmail.com>
 * @since 19-12-2019
 * @version 1.0
 * @return boolean Resultado de la validación de los campos
 */
function number_format(number, decimals, decPoint, thousandsSep) {
	number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
	var n = !isFinite(+number) ? 0 : +number
	var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
	var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
	var dec = (typeof decPoint === 'undefined') ? '.' : decPoint
	var s = ''

	var toFixedFix = function (n, prec) {
		var k = Math.pow(10, prec)
		return '' + (Math.round(n * k) / k)
		  .toFixed(prec)
	}

	// @todo: for IE parseFloat(0.55).toFixed(0) = 0;
	s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.')
	if (s[0].length > 3) {
		s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
	}

	if ((s[1] || '').length < prec) {
		s[1] = s[1] || ''
		s[1] += new Array(prec - s[1].length + 1).join('0')
	}

	return s.join(dec)
}

/**
 * Mostramos mensaje ante un posible error
 *
 * @author Harvey Sanchez <havo.sanchez@gmail.com>
 * @since 19-12-2019
 * @version 1.0
 * @return boolean Resultado de la validación de los campos
 */
function mensajeFail()
{
	var msg = 'Lo sentimos, se ha producido un inconveniente no controlado durante el tiempo de ejecuci&oacute;n.<br />';
	msg +=	'Intentelo nuevamente dentro de unos minutos.';

	$('button').attr('disabled', false);
	$('.alert-price').html(msg).fadeIn(1000);
	$('.alert-price').delay(4000).fadeOut('normal', function(){
		$(this).remove();
	});

	return false;
}