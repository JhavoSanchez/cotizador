<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <title>{{ config('app.name') }}</title>

		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />

		<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
		<meta http-equiv="Pragma" content="no-cache" />
		<meta http-equiv="Expires" content="0" />

	    <meta name="author" content="Interseguro" />
	    <meta name="keyword" content="Seguros" />
		<meta name="_token" content="{!! csrf_token() !!}" />

		<link rel="shortcut icon" href="{{ asset('img/enterprise/icon.png') }}">

		<!-- Supersized Css -->
        <link type="text/css" href="{{ asset('css/plugin/supersized/supersized.min.css') }}" rel="stylesheet">
		<!-- Bootstrap Core Css -->
		<link type="text/css" href="{{ asset('css/lib/bootstrap/bootstrap.min.css') }}" rel="stylesheet">
		<!-- Bootstrap DatePicker Css -->
        <link type="text/css" href="{{ asset('css/plugin/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet">
        <!-- Select2 Js -->
        <link type="text/css" href="{{ asset('css/plugin/select2/select2.min.css') }}" rel="stylesheet">
		<!-- Landing Css -->
        <link type="text/css" href="{{ asset('css/content/app.css') }}" rel="stylesheet">
    </head>
    <body class="price-page">
	    <div class="card">
	        <div class="header">
<!--
	            <div class="row">
					<div class="col-sm-12 mx-auto text-center">
						<img src="{{ asset('img/enterprise/logo.png') }}" width="200" alt="Logo Interseguro">
					</div>
	            </div>
-->
	            <div class="row">
	                <div class="col-sm-12">
	                	<h4 class="text-center">COTIZADOR DE SEGUROS DE VIAJES</h4>
	                </div>
	            </div>
	        </div>
	        <div class="body">
	        	<div class="price-form">
		            <form class="form-horizontal" name="form-cotizador" id="form-cotizador" method="post" role="form" novalidate>
						<div class="form-group">
							<div class="col-sm-12">
								<label for="destino">* Destino</label>
								<select class="form-control" id="destino" placeholder="Destino" required autofocus></select>
								<div class="invalid-feedback text-danger">Elija un destino</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<label for="numero-pasajero">* Nro. Pasajeros</label>
								<input type="text" class="form-control only-number" id="numero-pasajero" placeholder="Nro. Pasajero(s)" maxlength="2" autocomplete="off" required>
								<div class="invalid-feedback text-danger">Ingrese el número de pasajeros</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-6">
								<label for="fecha-ida">* Ida</label>
								<div class="input-group">
									<input type="text" class="form-control input-date" id="fecha-ida" placeholder="Ida" value="{{ date('d/m/Y') }}"  readonly required>
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
								<div class="invalid-feedback text-danger">Seleccione una fecha correcta.</div>
							</div>
							<div class="col-sm-6">
								<label for="fecha-vuelta">* Vuelta</label>
								<div class="input-group">
									<input type="text" class="form-control input-date" id="fecha-vuelta" placeholder="Vuelta" value="{{ date('d/m/Y') }}" readonly required>
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
								<div class="invalid-feedback text-danger">Seleccione una fecha correcta.</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<div class="alert alert-price alert-danger m-b-0" role="alert"></div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<button type="submit" class="btn btn-block btn-price"> Cotizar </button>
							</div>
						</div>
						<div class="form-group m-b-0">
							<div class="col-sm-12">
								<small class="text-danger">
									<strong>(*) Campos obligatorios</strong>
								</small>
							</div>
						</div>
					</form>
	        	</div>
	        	<div class="price-result">
					<table class="table table-bordered table-striped table-price-result"></table>
					<button type="submit" class="btn btn-block btn-return"> Regresar </button>
	        	</div>
	        </div>
	    </div>

    	<script type="text/javascript">
            var url = "{{ url('/') }}";
            var hoy = "{{ date('d/m/Y') }}";
        </script>

        <!-- Jquery Core Js -->
        <script type="text/javascript" src="{{ asset('js/lib/jquery/jquery.min.js') }}"></script>
        <!-- Supersized Js -->
        <script type="text/javascript" src="{{ asset('js/plugin/supersized/supersized.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/plugin/supersized/supersized-init.min.js') }}"></script>
        <!-- Bootstrap Core Js -->
        <script type="text/javascript" src="{{ asset('js/lib/bootstrap/bootstrap.min.js') }}"></script>
		<!-- Bootstrap DatePicker Js -->
		<script type="text/javascript" src="{{ asset('js/plugin/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/plugin/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js') }}"></script>
        <!-- Select2 Js -->
        <script type="text/javascript" src="{{ asset('js/plugin/select2/select2.full.min.js') }}"></script>
		<!-- Landing Js -->
        <script type="text/javascript" src="{{ asset('js/content/app.js') }}"></script>
    </body>
</html>