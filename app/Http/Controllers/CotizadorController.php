<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CotizadorController extends Controller
{
	/**
	 * Mostramos el formulario inicial
	 *
	 * @author Harvey Sanchez <havo.sanchez@gmail.com>
	 * @since 18-12-2019
	 * @version 1.0
	 */
	public function index()
	{
		return view('cotizador');
	}
}